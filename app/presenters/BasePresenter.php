<?php

namespace App\Presenters;

use Nette,
	App\Model;
use WebLoader\Nette\LoaderFactory;

/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
	/**
	 * @var LoaderFactory
	 * @inject
	 */
	public $webloader;

	protected function createComponentWebloaderCss() {
		return $this->webloader->createCssLoader('default');
	}


	protected function createComponentWebloaderJs() {
		return $this->webloader->createJavaScriptLoader('default');
	}


}
